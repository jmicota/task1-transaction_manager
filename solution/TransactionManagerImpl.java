/*
 * University of Warsaw
 * Concurrent Programming Course 2020/2021
 * Java Assignment
 *
 * Author: Justyna Micota 418427
 */

package cp1.solution;

import cp1.base.*;

import java.util.*;

public class TransactionManagerImpl implements TransactionManager {
    private final LocalTimeProvider timeProvider;
    private final SynchronizedResourceOrganizer resourceOrganizer;

    public TransactionManagerImpl(
              Collection<Resource> resources,
              LocalTimeProvider timeProvider) {
        this.timeProvider = timeProvider;
        this.resourceOrganizer = new SynchronizedResourceOrganizer(resources);
    }

    @Override
    public void startTransaction() throws AnotherTransactionActiveException {
        synchronized (resourceOrganizer) {
            if (resourceOrganizer.isActive(Thread.currentThread())) {
                throw new AnotherTransactionActiveException();
            } else {
                resourceOrganizer.startTransaction(Thread.currentThread(), timeProvider.getTime());
            }
        }
    }

    /** Check for cycles while trying to claim resource controlled by another Thread.
     * Interrupt a chosen Thread if cycle detected. */
    private synchronized void examineForeignResource(ResourceId rid) throws ActiveTransactionAborted {
        Long myTid = Thread.currentThread().getId();
        List<Long> cycle;

        cycle = resourceOrganizer.findCycle(rid, myTid);
        if (cycle != null) {
            Long threadToAbort = resourceOrganizer.findTransactionToAbort(cycle);
            Thread t = resourceOrganizer.setToAborted(threadToAbort);
            if (t != null) {
                resourceOrganizer.setToAborted(myTid);
                t.interrupt();
            }
        }
    }

    @Override
    public void operateOnResourceInCurrentTransaction(ResourceId rid, ResourceOperation operation)
            throws NoActiveTransactionException, UnknownResourceIdException, ActiveTransactionAborted,
                   ResourceOperationException, InterruptedException {

        Long myTid = Thread.currentThread().getId();
        if (!isTransactionActive())
            throw new NoActiveTransactionException();
        if (!resourceOrganizer.doesResourceExist(rid))
            throw new UnknownResourceIdException(rid);
        if (isTransactionAborted())
            throw new ActiveTransactionAborted();

        if (resourceOrganizer.whoControlsResource(rid) == null) {
            resourceOrganizer.tryToAcquireResource(rid, myTid); // throws InterruptedException
            resourceOrganizer.claimResource(rid, myTid);
        }
        else if (!myTid.equals(resourceOrganizer.whoControlsResource(rid))) {
            examineForeignResource(rid);
            //if (isTransactionAborted()) throw new ActiveTransactionAborted();
            resourceOrganizer.tryToAcquireResource(rid, myTid); // throws InterruptedException
            resourceOrganizer.claimResource(rid, myTid);
        }

        Resource r = resourceOrganizer.getResource(rid);
        try {
            r.apply(operation);
        } catch (ResourceOperationException e) {
            throw e;
        }
        resourceOrganizer.saveExecutedOperation(rid, operation);
    }

    @Override
    public void commitCurrentTransaction() throws NoActiveTransactionException, ActiveTransactionAborted {
        Thread t = Thread.currentThread();
        List<ResourceId> myResourcesIds;

        if (!resourceOrganizer.isActive(t))
            throw new NoActiveTransactionException();
        if (resourceOrganizer.isAborted(t))
            throw new ActiveTransactionAborted();
        myResourcesIds = resourceOrganizer.getMyResourcesIds(t.getId());

        resourceOrganizer.forgetTransaction(t);
        for(ResourceId rid : myResourcesIds) {
            resourceOrganizer.releaseResource(rid);
        }
        resourceOrganizer.forgetResources(t.getId());
    }

    @Override
    public void rollbackCurrentTransaction() {
        Thread t = Thread.currentThread();
        List<ResourceId> myResourcesIds;
        ArrayList<ResourceOperation> executedOperations;

        if (!isTransactionActive())
            return;
        myResourcesIds = resourceOrganizer.getMyResourcesIds(t.getId());

        for(ResourceId rid : myResourcesIds) {
            Resource r;
            r = resourceOrganizer.getResource(rid);
            executedOperations = resourceOrganizer.getExecutedOperations(rid);
            if (executedOperations != null) {
                for (ResourceOperation op : executedOperations) {
                    r.unapply(op);
                }
            }
        }

        resourceOrganizer.forgetTransaction(t);
        for(ResourceId rid : myResourcesIds) {
            resourceOrganizer.releaseResource(rid);
        }
        resourceOrganizer.forgetResources(t.getId());
    }

    @Override
    public boolean isTransactionActive() {
        return resourceOrganizer.isActive(Thread.currentThread());
    }

    @Override
    public boolean isTransactionAborted() {
        return resourceOrganizer.isAborted(Thread.currentThread());
    }
}
