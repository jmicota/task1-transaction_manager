/*
 * University of Warsaw
 * Concurrent Programming Course 2020/2021
 * Java Assignment
 *
 * Author: Justyna Micota 418427
 */

package cp1.solution;

import cp1.base.Resource;
import cp1.base.ResourceId;
import cp1.base.ResourceOperation;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;

public class SynchronizedResourceOrganizer {
    private final Collection<Resource> resources;
    private final Map<ResourceId, Long> whoHasRecourse;
    private final Map<Long, ResourceId> whatResourceThreadWaitsFor;
    private final Map<ResourceId, ArrayList<ResourceOperation>> executedOperations;
    private final List<Thread> activeThreads;
    private final List<Thread> abortedThreads;
    private final Map<Long, List<ResourceId>> threadsResources;
    private final Map<Long, Long> timeOfStart;
    private final Map<ResourceId, Semaphore> waitForResource;

    public SynchronizedResourceOrganizer(Collection<Resource> resources) {
        this.resources = Collections.synchronizedCollection(resources);

        whoHasRecourse = new ConcurrentHashMap<>();
        whatResourceThreadWaitsFor = new ConcurrentHashMap<>();
        executedOperations = new ConcurrentHashMap<>();
        threadsResources = new ConcurrentHashMap<>();
        timeOfStart = new ConcurrentHashMap<>();
        waitForResource = new ConcurrentHashMap<>();

        activeThreads = Collections.synchronizedList(new ArrayList<>());
        abortedThreads = Collections.synchronizedList(new ArrayList<>());

        for (Resource r : resources) {
            waitForResource.put(r.getId(), new Semaphore(1, true));
            executedOperations.put(r.getId(), new ArrayList<>());
        }
    }

    public Resource getResource(ResourceId rid) {
        for (Resource r : resources) {
            if (r.getId() == rid)
                return r;
        }
        return null;
    }

    /** Return list of Ids of Resources controlled by current Threads Transaction. */
    public List<ResourceId> getMyResourcesIds(Long tid) {
        return threadsResources.get(tid);
    }

    /** Save operation executed on specified Resource for a possible rollback. */
    public boolean doesResourceExist(ResourceId rid) {
        return getResource(rid) != null;
    }

    /** Returns Id of Thread that is in control of specified Resource. */
    public Long whoControlsResource(ResourceId rid) {
        return whoHasRecourse.getOrDefault(rid, null);
    }

    public boolean isActive(Thread t) {
        return activeThreads.contains(t);
    }

    public boolean isAborted(Thread t) {
        return abortedThreads.contains(t);
    }

    /** Initiate structures for new active thread in Organizer. */
    public synchronized void startTransaction(Thread t, Long time) {
        activeThreads.add(t);
        timeOfStart.put(t.getId(), time);
        threadsResources.put(t.getId(), new ArrayList<>());
    }

    /** Try to acquire resources semaphore and write down in
     * Organizer what Resource you're waiting for. */
    public void tryToAcquireResource(ResourceId rid, Long tid) throws InterruptedException {
        whatResourceThreadWaitsFor.put(tid, rid);
        try {
            waitForResource.get(rid).acquire();
        } catch (InterruptedException e) {
            throw e;
        }
    }

    /** Write down in Organizer that from now on this Resource is a part of this threads
     * Transaction. Initiate necessary structures. */
    public synchronized void claimResource(ResourceId rid, Long tid) {
        whatResourceThreadWaitsFor.remove(tid);
        whoHasRecourse.put(rid, tid);
        threadsResources.get(tid).add(rid);
        executedOperations.put(rid, new ArrayList<>());
    }

    /** Releases semaphore of specified resource and removes it's owner from Organizer. */
    public synchronized void releaseResource(ResourceId rid) {
        whoHasRecourse.remove(rid);
        waitForResource.get(rid).release();
    }

    /** Detect possible deadlock and return group of threads that would be affected by it.
     * Return null if no deadlock detected. */
    public synchronized List<Long> findCycle(ResourceId conflictingRid, Long tid) {
        List<Long> ret = new ArrayList<>();
        ret.add(tid);
        ResourceId subjectRid = conflictingRid;
        Long subjectOwner = whoHasRecourse.getOrDefault(subjectRid, null);

        while (subjectRid != null && subjectOwner != null && !subjectOwner.equals(tid)) {
            ret.add(subjectOwner);
            subjectRid = whatResourceThreadWaitsFor.getOrDefault(subjectOwner, null);
            if (subjectRid != null) {
                subjectOwner = whoHasRecourse.getOrDefault(subjectRid, null);
            }
            else subjectOwner = null;
        }
        if (subjectRid != null && subjectOwner != null) // cycle found
            return ret;
        else return null;
    }

    /** Find youngest transaction among those belonging to a group of threads. */
    public Long findTransactionToAbort(List<Long> cycle) {
        Long toAbort = cycle.get(0);
        Long timeToAbort = timeOfStart.getOrDefault(toAbort, null);
        for(Long tid : cycle) {
            if(timeOfStart.getOrDefault(tid, null) > timeToAbort) {
                timeToAbort = timeOfStart.getOrDefault(tid, null);
                toAbort = tid;
            }
            else if (timeOfStart.getOrDefault(tid, null) == timeToAbort) {
                toAbort = (tid > toAbort ? tid : toAbort);
            }
        }
        return toAbort;
    }

    public Thread setToAborted(Long tid) {
        for (Thread t : activeThreads) {
            if (t.getId() == tid) {
                abortedThreads.add(t);
                return t;
            }
        }
        return null;
    }

    /** Returns reversed list of ResourceOperations executed on specified Resource
     * during current transaction. */
    public ArrayList<ResourceOperation> getExecutedOperations(ResourceId rid) {
        ArrayList<ResourceOperation> reversedListOfOperations = new ArrayList<>();
        ArrayList<ResourceOperation> operations = executedOperations.getOrDefault(rid, null);
        if (operations == null) return null;
        for (int i = operations.size() - 1; i >= 0; i--) {
            reversedListOfOperations.add(operations.get(i));
        }
        return reversedListOfOperations;
    }

    /** Save operation executed on specified Resource for a possible rollback. */
    public void saveExecutedOperation(ResourceId rid, ResourceOperation op) {
        executedOperations.get(rid).add(op);
    }

    /** Delete all data concerning this Threads Transaction from Organizer,
     * except of structure remembering its resources, which will be deleted
     * separately after releasing them. */
    public synchronized void forgetTransaction(Thread t) {
        if(isActive(t)) {
            activeThreads.remove(t);
            if (isAborted(t))
                abortedThreads.remove(t);
            timeOfStart.remove(t.getId());
            for(ResourceId rid : threadsResources.get(t.getId())) {
                executedOperations.remove(rid);
            }
        }
    }

    /** Forget resources involved in transaction. */
    public void forgetResources(Long tid) {
        threadsResources.remove(tid);
    }
}
